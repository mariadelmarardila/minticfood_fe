import axios from 'axios';

const API_URL = "https://misionticfood-be.herokuapp.com";

axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem("token_access");

class ProductoService {

    getAll() {
        return axios.get(API_URL + "/allproductos/");
    }

    //get(id) {
    //    return http.get(`/productos/${id}/`);
    //}

    create(data) {
        return axios.post(API_URL + "/productos/", data);
    }

    update(id, data) {
        return axios.put(API_URL + `/updateproductos/${id}/`, data);
    }

    delete(id) {
        return axios.delete(API_URL + `/deleteproductos/${id}/`);
    }
}

export default new ProductoService();